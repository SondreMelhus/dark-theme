import React, { useState } from 'react';
import FunctionContextComponent from './FunctionContextComponent'
import ClassContextComponent from './ClassContextComponent'
import { ThemeProvider } from './ThemeContext'

export const ThemeContext = React.createContext();

export function App() {
  return (
    <div>
      <ThemeProvider>
        <FunctionContextComponent />
      </ThemeProvider>
    </div>
  );
}

export default App;
